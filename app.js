const express = require('express');
var app = express();
//import express  from 'express';  
//createing a new express application

const userRouter = require('./routes/userRoutes.js');


app.use('/', userRouter);


app.listen(3003, () => {
    console.log('port running at 3003');
});