const express = require('express');
var router = express.Router();
const getAll = require('../controllers/userController.js');

router.get('/users/list', getAll)

module.exports = router;