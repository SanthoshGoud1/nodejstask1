const fetch = require("node-fetch");
const _ = require("lodash");

async function getAll(req, res) {
    const count = req.query.count || 10;
    const arr = []
    if (count > 50 || count < 0) {
        res.send('Error max fifty limite min should be one')
    }
    else {
        const userJson = await fetch('https://randomuser.me/api/?results=' + count)
        var jsonData = await userJson.json();
        
            jsonData.results.forEach(({ gender, name: { title, first, last }, email, phone, dob: { date, age },
            location: { street: { number, name }, city, state, postcode } } = jsonData.results) => {

            const fullname = first + ' ' + last;
            const dateUpdate = new Date(date);
            const dob = dateUpdate.getFullYear() + '-' + (dateUpdate.getMonth() + 1) + '-' + dateUpdate.getDate();
            const address = { line1: number, line2: name, city: city, state: state, zip: postcode }

            const obj = { gender, title, fullname, email, phone, dob, age, address }
            arr.push(obj)


        });


        const objAfterSoring = _.orderBy(arr, ['name', 'age'], ['asc', 'desc']);
        res.send(objAfterSoring)



    };


};

module.exports = getAll;

